import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CreateMealComponent } from './create-meal/create-meal.component';
import { MealsResolverService } from './home/mealsResolverService';
import { HomeComponent } from './home/home.component';

const routes: Routes = [
  {path: '', component: HomeComponent},
  {path: 'home', component: HomeComponent},
  {path: 'add', component: CreateMealComponent },
  {path: ':id/edit',
    component: CreateMealComponent,
    resolve: {meal: MealsResolverService}
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
