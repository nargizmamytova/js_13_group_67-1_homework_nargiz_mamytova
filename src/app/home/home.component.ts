import { Component, OnInit } from '@angular/core';
import { Meal } from '../shared/meal.model';
import { MealService } from '../shared/meal.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  allMeals!: Meal[];
  mealsChangeSubscription!: Subscription;
  mealsFetchingSubscription!: Subscription;
  loading: boolean = false;
  totalCalories: number = 0;
  constructor(private mealService: MealService) { }

  ngOnInit(): void {

    this.allMeals = this.mealService.getMeals();
    this.mealsChangeSubscription = this. mealService.mealsChange.subscribe((meals: Meal[]) => {
      this.allMeals = meals;
     this.getTotalCalories();
    });
    this.mealsFetchingSubscription = this.mealService.mealsFetching.subscribe((isFetching: boolean) => {
      this.loading = isFetching;
    });

    this.mealService.fetchAllMeals();

  }
  getTotalCalories(){
    this.totalCalories = 0;
    return this.allMeals.forEach(meal => {
       this.totalCalories += meal.calorie;
     })

  }
}
