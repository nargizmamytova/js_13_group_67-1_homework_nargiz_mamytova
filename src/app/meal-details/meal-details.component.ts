import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import { Meal } from '../shared/meal.model';
import { Subscription } from 'rxjs';
import { ActivatedRoute, Data, Router } from '@angular/router';
import { MealService } from '../shared/meal.service';

@Component({
  selector: 'app-meal-details',
  templateUrl: './meal-details.component.html',
  styleUrls: ['./meal-details.component.css']
})
export class MealDetailsComponent implements OnInit {
  @Input()meal!: Meal;
  isRemoving = false;

  constructor(
    private route: ActivatedRoute,
    private mealService: MealService,
    private router: Router
  ) { }

  ngOnInit(): void {
  }

  onRemove() {
    this.mealService.removeMeal(this.meal.id).subscribe(() => {
      this.mealService.fetchAllMeals();
      void this.router.navigate(['/'])
    })
  }

}
