import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { MealService } from '../shared/meal.service';
import { Subscription } from 'rxjs';
import { ActivatedRoute, Router } from '@angular/router';
import { Meal } from '../shared/meal.model';

@Component({
  selector: 'app-create-meal',
  templateUrl: './create-meal.component.html',
  styleUrls: ['./create-meal.component.css']
})
export class CreateMealComponent implements OnInit, OnDestroy {
  @ViewChild('f') form!: NgForm;
  isEdit = false;
  editedId = '';
  isUpLoading = false;

  mealUpLoadingSubscription!: Subscription;

  constructor(
    private mealService: MealService,
    private router: Router,
    private route: ActivatedRoute
  ) { }

  ngOnInit(): void {
    this.mealUpLoadingSubscription = this.mealService.mealsUpLoading.subscribe((isUpLoading: boolean) => {
      this.isUpLoading = isUpLoading;
    });
    this.route.data.subscribe(data => {
      const meal = <Meal | null>data.meal;
      if(meal){
        this.isEdit = true;
        this.editedId = meal.id;
        this.setFormValue({
          time: meal.time,
          dish: meal.dish,
          calorie: meal.calorie
        })
      }else{
        this.isEdit = false;
        this.editedId = '';
        this.setFormValue({
          time: '',
          dish: '',
          calorie: '',
        })
      }
    })
  }
  setFormValue(value: {[key: string]: any}){
    setTimeout(() => {
      this.form.form.setValue(value);
    })
  }
  createMeal() {
    const id = this.editedId || Math.random().toString();
    const meal = new Meal(
      id,
      this.form.value.time,
      this.form.value.dish,
      this.form.value.calorie
    );
    const next = () => {
      this.mealService.fetchAllMeals();
      void this.router.navigate(['/']);
     };
    if(this.isEdit){
       this.mealService.editMeal(meal).subscribe(next);
    }else {
      this.mealService.addMeal(meal).subscribe(next)
    }
  }
  ngOnDestroy() {
    this.mealUpLoadingSubscription.unsubscribe();
  }
}
