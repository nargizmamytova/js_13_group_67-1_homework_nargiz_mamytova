import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { map, tap } from 'rxjs/operators';
import { Meal } from './meal.model';

@Injectable()
export class MealService{
  mealsChange = new Subject<Meal[]>();
  mealsFetching = new Subject<boolean>();
  mealsUpLoading = new Subject<boolean>();
  mealsRemoving = new Subject<boolean>();

  private meals : Meal[] = [];
  constructor(
    private http: HttpClient
  ) {}
  getMeals(){
    return this.meals.slice();
  }
  addMeal(meal: Meal){
    const body = {
      time: meal.time,
      dish: meal.dish,
      calorie: meal.calorie
    };
    this.mealsUpLoading.next(true);
    return this.http.post('https://plovo-bb6ec-default-rtdb.firebaseio.com/meals.json', body)
      .pipe(tap(() => {
        this.mealsUpLoading.next(false);
      },() => {
          this.mealsUpLoading.next(false);
      }))
  }
  editMeal(meal: Meal){
    this.mealsUpLoading.next(true);
    const body = {
      time: meal.time,
      dish: meal.dish,
      calorie: meal.calorie
    };
    return this.http.put
    (`https://plovo-bb6ec-default-rtdb.firebaseio.com/meals/${meal.id}.json`, body)
      .pipe(tap(() => {
          this.mealsUpLoading.next(false)
        }, () => {
          this.mealsUpLoading.next(false)
        }
      ))

  }
  fetchMeal(id: string){
    return this. http.get<Meal>(`https://plovo-bb6ec-default-rtdb.firebaseio.com/meals/${id}.json`)
      .pipe(map(result => {
        if(!result){
          return null
        }
        return new Meal(
          id,
          result.time,
          result.dish,
          result.calorie
        )
      }))
  }
  fetchAllMeals(){
    this.mealsFetching.next(true)
    this.http.get<{[id: string]: Meal}>
    ('https://plovo-bb6ec-default-rtdb.firebaseio.com/meals.json')
      .pipe(map(result => {
        return Object.keys(result).map(id => {
          const mealsData = result[id]
          return new Meal (
            id,
            mealsData.time, mealsData.dish,
            mealsData.calorie
          )
        })
      }))
      .subscribe(meals => {
        this.meals = meals;
        this.mealsChange.next(this.meals.slice())
        this.mealsFetching.next(false)
      }, error => {
        this.mealsFetching.next(false)
      })
  }
  removeMeal(id: string){
    this.mealsRemoving.next(true);
    return this.http.delete(`https://plovo-bb6ec-default-rtdb.firebaseio.com/meals/${id}.json`)
      .pipe(tap(() => {
        this.mealsRemoving.next(false);
      },() => {
        this.mealsRemoving.next(false);
      }))
  }

}
