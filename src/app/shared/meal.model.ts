export class Meal{
  constructor(
    public id: string,
    public time: string,
    public dish: string,
    public calorie: number
  ) {}
}
